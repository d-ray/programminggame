/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using ProgrammingGame.UI;

namespace ProgrammingGame
{
    /// <summary>
    /// Handles the level scene
    /// </summary>
    public class LevelHandlerScene : BaseScene
    {
        #region Variables

        [SerializeField]
        private GameObject m_DefaultLevel;

        [SerializeField]
        private GameObject[] m_Levels;

        [SerializeField]
        private Toggle m_UIPlayPauseToggle;

        [SerializeField]
        private Button m_UIStopButton;

        [SerializeField]
        private Toggle m_UIStepToggle;

        [SerializeField]
        private Image m_StepIndicatorUI;

        [SerializeField]
        private RectTransform m_UIProgramViewTransform;

        [SerializeField]
        private RectTransform m_UIProgramContentTransform;

        [SerializeField]
        private GameObject m_UIProgramLockObject;

        [SerializeField]
        private ModalWindowManager m_ModalWindowManager;

        [SerializeField]
        private RectTransform m_UIBotStackTransform;

        [SerializeField]
        private Text m_UICommandCountText;

        [SerializeField]
        private Transform m_UIAvailableCommandsTransform;

        [SerializeField]
        private Text m_LevelNumberText;

        /// <summary>
        /// Reference of the currently loaded level
        /// </summary>
        private Level m_CurrentLevel;

        public delegate void OnLevelEndDelegate();

        public static event OnLevelEndDelegate OnLevelEnd;

        #endregion Variables

        public RectTransform UIProgramViewTransform
        {
            get { return m_UIProgramViewTransform; }
        }

        public RectTransform UIProgramContentTransform
        {
            get { return m_UIProgramContentTransform; }
        }

        public GameObject UIProgramLockObject
        {
            get { return m_UIProgramLockObject; }
        }

        public ModalWindowManager ModalWindowManager
        {
            get { return m_ModalWindowManager; }
        }

        public RectTransform UIBotStack
        {
            get { return m_UIBotStackTransform; }
        }

        public int LevelMaxCommands
        {
            get { return m_CurrentLevel.MaxCommands; }
        }

        public int CurrentCommandCount
        {
            get { return m_UIProgramContentTransform.childCount; }
        }

        #region Unity Methods

        // Initialize the singletons
        protected override void Awake()
        {
            base.Awake();
        }

        // Execute initialization sequence of the level handler
        private void Start()
        {
            m_UIProgramContentTransform.anchoredPosition = new Vector3(0.0f, 0.0f);

            // debug set level
            //Session.Instance.CurrentLevel = 0;
            Session.Instance.LevelCount = m_Levels.Length;

            StartCoroutine(InitSequence());
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                m_CurrentLevel.StopSimulation();
                m_ModalWindowManager.ShowWindow(1);
            }
        }

        #endregion Unity Methods

        private void InitUI()
        {
            m_UIPlayPauseToggle.interactable = false;
            m_UIStopButton.interactable = false;
            m_UIStepToggle.interactable = false;
            m_UIProgramLockObject.SetActive(false);
            m_StepIndicatorUI.fillAmount = 0.0f;
        }

        private void LockAllCommands()
        {
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandForward>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandBackward>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRotateL>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRotateR>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRepeatProgram>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpOffset>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpTrue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpFalse>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareZero>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareEqual>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareGreater>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareGreaterEqual>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandPushValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandPopValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDuplicateValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandSwapValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandIncrementValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDecrementValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandAddValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandSubtractValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandMultiplyValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDivideValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandModuloValue>(true).gameObject.SetActive(false);
            m_UIAvailableCommandsTransform.GetComponentInChildren<CommandNegateValue>(true).gameObject.SetActive(false);

        }

        private void SetUnlockedCommands(UnlockedCommands cmds)
        {
            if (cmds.movements)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandForward>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandBackward>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRotateL>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRotateR>(true).gameObject.SetActive(true);
            }

            if (cmds.programRepetition)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandRepeatProgram>(true).gameObject.SetActive(true);
            }

            if (cmds.jumps)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpOffset>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpTrue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandJumpFalse>(true).gameObject.SetActive(true);
            }

            if (cmds.comparators)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareZero>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareEqual>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareGreater>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandCompareGreaterEqual>(true).gameObject.SetActive(true);
            }

            if (cmds.stackManipulation)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandPushValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandPopValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDuplicateValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandSwapValue>(true).gameObject.SetActive(true);
            }

            if (cmds.maths)
            {
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandIncrementValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDecrementValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandAddValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandSubtractValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandMultiplyValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandDivideValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandModuloValue>(true).gameObject.SetActive(true);
                m_UIAvailableCommandsTransform.GetComponentInChildren<CommandNegateValue>(true).gameObject.SetActive(true);
            }
        }

        public void UpdateCommandCountText()
        {
            m_UICommandCountText.text = "Commands: " + m_UIProgramContentTransform.childCount + "/" + m_CurrentLevel.MaxCommands;

            if (m_UIProgramContentTransform.childCount >= m_CurrentLevel.MaxCommands)
                m_UICommandCountText.color = Color.red;
            else
                m_UICommandCountText.color = Color.white;
        }

        /// <summary>
        /// Initialization sequence
        /// </summary>
        /// <returns></returns>
        private IEnumerator InitSequence()
        {
            // set settings that presist across levels here (assuming the player not going back to main menu)

            yield return StartCoroutine(InitScene());
        }

        /// <summary>
        /// Initializes the scene (also destroy the currently loaded scene with new one)
        /// </summary>
        /// <returns></returns>
        private IEnumerator InitScene()
        {
            Camera.main.transform.position = new Vector3(0.0f, 0.0f, -10.0f);

            InitUI();

            // set settings that reset during reload of level here
            for (int i = 0; i < m_UIProgramContentTransform.childCount; i++)
            {
                Destroy(m_UIProgramContentTransform.GetChild(i).gameObject);
            }

            // destroy previous level and load new level
            if (m_CurrentLevel != null)
            {
                Destroy(m_CurrentLevel.gameObject);
                m_CurrentLevel = null;
            }

            yield return null;

            var levelId = Session.Instance.SessionLoaded ? Session.Instance.CurrentLevel : -1;

            var instance = Instantiate(levelId < m_Levels.Length ? m_Levels[levelId] : m_DefaultLevel);
            m_CurrentLevel = instance.GetComponent<Level>();
            m_CurrentLevel.PlayPauseToggleUI = m_UIPlayPauseToggle;
            m_CurrentLevel.StopButtonUI = m_UIStopButton;
            m_CurrentLevel.StepIndicatorUI = m_StepIndicatorUI;
            m_CurrentLevel.ProgramContentUI = m_UIProgramContentTransform;
            m_CurrentLevel.ProgramLockUI = m_UIProgramLockObject;
            m_CurrentLevel.BotStackUI = m_UIBotStackTransform;
            m_CurrentLevel.WindowManager = m_ModalWindowManager;

            LockAllCommands();
            SetUnlockedCommands(m_CurrentLevel.UnlockedCommands);

            m_CurrentLevel.LevelInit();

            m_LevelNumberText.text = "LEVEL " + (levelId + 1);
            UpdateCommandCountText();

            m_UIPlayPauseToggle.interactable = true;
            m_UIStepToggle.interactable = true;
            m_UIStepToggle.isOn = false;
        }

        #region Level Elements Events

        // put all event methods here

        public void ClearProgram()
        {
            m_ModalWindowManager.ShowWindow(3);
        }

        public void ToggleSimulation(bool isOn)
        {

            if (isOn)
            {
                m_CurrentLevel.PlaySimulation();
                //m_UIStopButton.interactable = true;
            }
            else
            {
                m_CurrentLevel.PauseSimulation();
            }
        }

        public void StopSimulation()
        {
            //m_UIPlayPauseToggle.isOn = false;
            //m_UIStopButton.interactable = false;
            m_CurrentLevel.StopSimulation();
        }

        public void ToggleStep(bool isOn)
        {
            m_CurrentLevel.ToggleStepMode(isOn);
        }

        public void QuitButtonPress()
        {
            m_CurrentLevel.StopSimulation();
            m_ModalWindowManager.ShowWindow(1);
        }

        public void RestartLevel()
        {
            // should show restart confirm dialog

            if (OnLevelEnd != null)
                OnLevelEnd();

            StartCoroutine(InitScene());
        }

        public void LoadNextLevel()
        {
            if (OnLevelEnd != null)
                OnLevelEnd();

            // loads the next level
            Session.Instance.PlayNext();
            Session.Instance.SaveSession();
            StartCoroutine(InitScene());
        }

        #endregion Level Elements Events
    }
}