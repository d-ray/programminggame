﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ProgrammingGame.UI;

namespace ProgrammingGame
{
    public class MainMenuScene : BaseScene
    {
        [SerializeField]
        private ModalWindowManager m_WindowManager;

        [SerializeField]
        private Button m_Continuebutton;

        private void Start()
        {
            Session.Instance.LoadSession();

            if (Session.Instance.SessionLoaded)
            {
                m_Continuebutton.interactable = true;
            }
        }

        public void NewGame()
        {
            Session.Instance.NewSession();
            GoToScene(Scene.LevelHandler);
        }

        public void ContinueGame()
        {
            GoToScene(Scene.LevelHandler);
        }

        public void QuitGame()
        {
            m_WindowManager.ShowWindow(0);
        }
    }
}