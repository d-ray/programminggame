﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame.FX
{
    public class BackgroundScroller : MonoBehaviour
    {
        [SerializeField]
        private Vector3 m_ScrollSpeed;
        [SerializeField]
        private Vector3 m_ScrollLimit;

        private void Start()
        {
            //Application.targetFrameRate = 10;
            m_ScrollLimit.x = Mathf.Abs(m_ScrollLimit.x);
            m_ScrollLimit.y = Mathf.Abs(m_ScrollLimit.y);
            m_ScrollLimit.z = 0f;
        }

        private void Update()
        {
            transform.localPosition += m_ScrollSpeed * Time.deltaTime;

            float xcPos;
            if (transform.localPosition.x > m_ScrollLimit.x)
            {
                xcPos = transform.localPosition.x - m_ScrollLimit.x;
                transform.localPosition = new Vector3(-transform.localPosition.x + xcPos, transform.localPosition.y);
            }
            else if (transform.localPosition.x < -m_ScrollLimit.x)
            {
                xcPos = transform.localPosition.x - m_ScrollLimit.x;
                transform.localPosition = new Vector3(transform.localPosition.x - xcPos, transform.localPosition.y);
            }

            if (transform.localPosition.y > m_ScrollLimit.y)
            {
                xcPos = transform.localPosition.y - m_ScrollLimit.y;
                transform.localPosition = new Vector3(transform.localPosition.x, -transform.localPosition.y + xcPos);
            }
            else if (transform.localPosition.y < -m_ScrollLimit.y)
            {
                xcPos = transform.localPosition.y - m_ScrollLimit.y;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - xcPos);
            }
        }
    }
}