/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using ProgrammingGame.UI;

namespace ProgrammingGame
{
    /// <summary>
    /// Possible state of the level
    /// </summary>
    public enum LevelState
    {
        // the simulation is not playing
        Ready,

        // the simulation is in progress
        Simulating,

        // the simulation is paused
        Paused,

        // Goal has been achieved
        SimulationEnded
    }

    [System.Serializable]
    public class UnlockedCommands
    {
        public bool movements;
        public bool programRepetition;
        public bool jumps;
        public bool comparators;
        public bool stackManipulation;
        public bool maths;
    }

    // The level simulation part of the Level Class
    /// <summary>
    /// Represents a level that responsible for handling level objects
    /// </summary>
    public class Level : MonoBehaviour
    {
        #region Constants

        /// <summary>
        /// Possible simulation states
        /// </summary>
        private enum SimulationState
        {
            InitStep,
            // the step is about to start
            BeginStep,

            // currently stepping
            Stepping,

            // the end of step
            EndStep
        }

        #endregion Constants

        #region Variables

        Grid m_LevelGrid;

        [SerializeField]
        Tilemap m_FloorMap;

        [SerializeField]
        Tilemap m_WallMap;

        [SerializeField]
        Vector3Int m_FinishPosition;

        [SerializeField]
        int m_MaxCommands;

        [SerializeField]
        UnlockedCommands m_UnlockedCommands;

        Dictionary<int, Dictionary<int, CellObject>> m_CellObjs;
        Dictionary<CellObject, Vector3Int> m_CellObjPositions;

        private Toggle m_PlayPauseToggleUI;
        private Button m_StopButtonUI;
        private Image m_StepIndicatorUI;
        private RectTransform m_ProgramContentUI;
        private GameObject m_ProgramLockUI;
        private RectTransform m_BotStackUI;
        private ModalWindowManager m_WindowManager;
        private PlayerBot m_PlayerBot;

        /// <summary>
        /// Simulation speed multiplier
        /// </summary>
        public float stepSpeed = 1f;

        /// <summary>
        /// The current state of the level
        /// </summary>
        private LevelState m_LevelState;

        /// <summary>
        /// The current state of the simulation
        /// </summary>
        private SimulationState m_SimulationState;

        /// <summary>
        /// The current progress of a step (0 to 1) for use as progress param in OnStep() invocation
        /// </summary>
        private float m_CurStepTime;

        /// <summary>
        /// Keeps track the number of passed steps in simulation
        /// </summary>
        private int m_StepCount;

        private bool m_UpdateOnInitStep;
        private bool m_StepMode;

        /// <summary>
        /// Gets the level state
        /// </summary>
        public LevelState State
        {
            get { return m_LevelState; }
        }


        /// <summary>
        /// Gets the number of step taken by the simulation
        /// </summary>
        public int StepCount
        {
            get { return m_StepCount; }
        }

        public Grid Grid
        {
            get { return m_LevelGrid; }
        }

        public Tilemap FloorMap
        {
            get { return m_FloorMap; }
        }

        public Tilemap WallMap
        {
            get { return m_WallMap; }
        }

        public Toggle PlayPauseToggleUI
        {
            set { m_PlayPauseToggleUI = value; }
        }

        public Button StopButtonUI
        {
            set { m_StopButtonUI = value; }
        }

        public Image StepIndicatorUI
        {
            set { m_StepIndicatorUI = value; }
        }

        public RectTransform ProgramContentUI
        {
            get { return m_ProgramContentUI; }
            set { m_ProgramContentUI = value; }
        }

        public GameObject ProgramLockUI
        {
            set { m_ProgramLockUI = value; }
        }

        public RectTransform BotStackUI
        {
            get { return m_BotStackUI; }
            set { m_BotStackUI = value; }
        }

        public int MaxCommands
        {
            get { return m_MaxCommands; }
        }

        public UnlockedCommands UnlockedCommands
        {
            get { return m_UnlockedCommands; }
        }

        public ModalWindowManager WindowManager
        {
            set { m_WindowManager = value; }
        }

        /// <summary>
        /// Delegate for just before a step in simulation
        /// </summary>
        public delegate void OnPreStepDelegate();

        /// <summary>
        /// Event for just before a step in simulation
        /// </summary>
        public event OnPreStepDelegate OnPreStep;

        /// <summary>
        /// Delegate for when a step is about to start
        /// </summary>
        public delegate void OnStepStartDelegate();

        /// <summary>
        /// Event for when a step is about to start (Called after OnPreStep)
        /// </summary>
        public event OnStepStartDelegate OnStepStart;

        /// <summary>
        /// Delegate for every frame update during a step
        /// </summary>
        /// <param name="progress"></param>
        public delegate void OnStepDelegate(float progress);

        /// <summary>
        /// Event for every frame update during a step
        /// </summary>
        public event OnStepDelegate OnStepTick;

        public delegate void OnStepEndDelegate();
        public event OnStepEndDelegate OnStepEnd;

        /// <summary>
        /// Delegate for after a step has finished
        /// </summary>
        public delegate void OnPostStepDelegate();

        /// <summary>
        /// Event for after a step has finished
        /// </summary>
        public event OnPostStepDelegate OnPostStep;

        /// <summary>
        /// Delegate for when simulation is entering playing state.
        /// </summary>
        public delegate void OnSimulationPlayDelegate();

        /// <summary>
        /// Invoked when PlaySimulation() is called
        /// </summary>
        public event OnSimulationPlayDelegate OnSimulationPlay;

        /// <summary>
        /// Delegate for when simulation is entering stopped state
        /// </summary>
        public delegate void OnSimulationStopDelegate();

        /// <summary>
        /// Invoked when StopSimulation() is called
        /// </summary>
        public event OnSimulationStopDelegate OnSimulationStop;

        /// <summary>
        /// Delegate for when simulation is entering paused state
        /// </summary>
        public delegate void OnSimulationPauseDelegate();

        /// <summary>
        /// Invoked when PauseSimulation() is called
        /// </summary>
        public event OnSimulationPauseDelegate OnSimulationPause;

        /// <summary>
        /// Delegate for when simulation is entering playing state after paused state
        /// </summary>
        public delegate void OnSimulationResumeDelegate();

        /// <summary>
        /// Invoked when PlaySimulation() is called and was in paused state
        /// </summary>
        public event OnSimulationResumeDelegate OnSimulationResume;

        public delegate void OnSimulationEndDelegate();
        public event OnSimulationEndDelegate OnSimulationEnd;

        #endregion Variables

        #region Unity Methods

        // Initialize variables and states
        public void LevelInit()
        {
            m_LevelGrid = GetComponent<Grid>();

            Debug.Assert(m_FloorMap != null, "No floor tilemap!");
            Debug.Assert(m_WallMap != null, "No wall tilemap!");

            m_CellObjs = new Dictionary<int, Dictionary<int, CellObject>>();
            m_CellObjPositions = new Dictionary<CellObject, Vector3Int>();

            m_StepCount = 0;
            SubscribeToLevelHandlerEvents();
            Init();
        }

        // Main simulation logic per frame
        private void Update()
        {
            // does nothing if Ready, Paused and AchievedGoal
            if (m_LevelState == LevelState.Ready || m_LevelState == LevelState.Paused || m_LevelState == LevelState.SimulationEnded)
                return;

            // else...
            if (m_LevelState == LevelState.Simulating)
            {
                switch (m_SimulationState)
                {
                    case SimulationState.InitStep:
                        m_CurStepTime = 0f;

                        if (OnPreStep != null)
                            OnPreStep();

                        if (m_StepIndicatorUI)
                        {
                            m_StepIndicatorUI.fillAmount = 0.0f;
                        }

                        if (!m_UpdateOnInitStep)
                        {
                            m_SimulationState = SimulationState.BeginStep;
                            goto case SimulationState.BeginStep;
                        }

                        m_SimulationState = SimulationState.InitStep;
                        m_UpdateOnInitStep = false;

                        if (m_StepMode)
                        {
                            m_PlayPauseToggleUI.isOn = false;
                            m_LevelState = LevelState.Paused;
                        }

                        break;

                    // things to do when a step is about to start
                    case SimulationState.BeginStep:
                        if (OnStepStart != null)
                            OnStepStart();

                        // stepping will start next frame...
                        m_SimulationState = SimulationState.Stepping;

                        break;

                    // things to do when currently stepping
                    case SimulationState.Stepping:
                        StepTick();

                        if (m_StepIndicatorUI)
                        {
                            m_StepIndicatorUI.fillAmount = m_CurStepTime;
                        }

                        break;

                    // things to do when a step has ended
                    case SimulationState.EndStep:
                        if (OnStepEnd != null)
                            OnStepEnd();

                        if (OnPostStep != null)
                            OnPostStep();

                        // count the step
                        m_StepCount++;

                        if (m_StepIndicatorUI)
                        {
                            m_StepIndicatorUI.fillAmount = 1.0f;
                        }

                        // begin again
                        m_SimulationState = SimulationState.InitStep;

                        if (m_StepMode)
                        {
                            m_PlayPauseToggleUI.isOn = false;
                            m_LevelState = LevelState.Paused;
                        }

                        if (m_LevelGrid.LocalToCell(m_PlayerBot.transform.localPosition) == m_FinishPosition)
                        {
                            EndSimulation();
                            if (Session.Instance.HasNext())
                            {
                                Session.Instance.PlayNext();
                                Session.Instance.SaveSession();
                                m_WindowManager.ShowWindow(2);
                            }
                            else
                            {
                                Session.Instance.DeleteSession();
                                m_WindowManager.ShowWindow(4);
                            }
                            Debug.Log("Player win");
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        private void OnDestroy()
        {
            UnsubscribeFromLevelHandlerEvents();
        }

        #endregion Unity Methods

        public CellObject GetCellObjectAt(Vector3Int position, bool initNull = false)
        {
            // we need to check whether the table has x and y dictionary
            // to prevent null exception
            // lets check the x dictionary first...
            Dictionary<int, CellObject> dictY;
            if (!m_CellObjs.TryGetValue(position.x, out dictY))
            {
                // m_CellObjs is x
                if (initNull)
                {
                    // y dictionary for current x doesnt exist yet, initialize...
                    dictY = new Dictionary<int, CellObject>();
                    m_CellObjs.Add(position.x, dictY);
                }
                else
                {
                    return null;
                }
            }

            // ...now lets check the y dictionary...
            CellObject obj;
            if (!dictY.TryGetValue(position.y, out obj))
            {
                return null;
            }

            return obj;
        }

        public void SyncCellObjectPosition(CellObject obj)
        {
            var newVec = m_LevelGrid.LocalToCell(obj.transform.position);

            // has previously synced
            if (m_CellObjPositions.ContainsKey(obj))
            {
                // remove object from the cellobject table
                m_CellObjs[m_CellObjPositions[obj].x].Remove(m_CellObjPositions[obj].y);

                // then update the positions dictionary
                m_CellObjPositions[obj] = newVec;
            }
            else
            {
                // not previously synced
                // add object to positions dictionary
                m_CellObjPositions.Add(obj, newVec);
            }

            Dictionary<int, CellObject> dictY;
            if (!m_CellObjs.TryGetValue(newVec.x, out dictY))
            {
                // y dictionary for current x doesnt exist yet, initialize...
                dictY = new Dictionary<int, CellObject>();
                m_CellObjs.Add(newVec.x, dictY);
            }

            // add object to cellobject table
            dictY.Add(newVec.y, obj);
        }

        public bool RemoveCellObject(CellObject obj)
        {
            if (!m_CellObjPositions.ContainsKey(obj))
            {
                return false;
            }

            m_CellObjs[m_CellObjPositions[obj].x].Remove(m_CellObjPositions[obj].y);
            m_CellObjPositions.Remove(obj);

            return true;
        }

        #region Simulation Functions

        public void RepeatInitStep()
        {
            m_UpdateOnInitStep = true;
        }

        /// <summary>
        /// Start or resume the simulation
        /// </summary>
        public void PlaySimulation()
        {
            if (m_LevelState != LevelState.Simulating)
            {
                m_StopButtonUI.interactable = true;
                m_ProgramLockUI.SetActive(true);

                // if ready (not from Paused state)
                if (m_LevelState == LevelState.Ready)
                {
                    // put things to do before started here
                    m_StepCount = 0;

                    if (OnSimulationPlay != null)
                        OnSimulationPlay();
                }
                // ... from Paused state
                else if (m_LevelState == LevelState.Paused)
                {
                    // put things to do before resumed here

                    if (OnSimulationResume != null)
                        OnSimulationResume();
                }

                // start the simulation
                m_LevelState = LevelState.Simulating;
            }
        }

        /// <summary>
        /// Stop the simulation
        /// </summary>
        public void StopSimulation()
        {
            if (m_LevelState != LevelState.Ready)
            {
                // put things to do before stopped here
                m_PlayPauseToggleUI.interactable = true;
                m_PlayPauseToggleUI.isOn = false;
                m_StopButtonUI.interactable = false;
                m_ProgramLockUI.SetActive(false);

                if (OnSimulationStop != null)
                    OnSimulationStop();

                Init();
            }
        }

        /// <summary>
        /// Pause the simulation
        /// </summary>
        public void PauseSimulation()
        {
            if (m_LevelState != LevelState.Paused)
            {
                // put things to do before paused here

                if (OnSimulationPause != null)
                    OnSimulationPause();

                // pause the simulation
                m_LevelState = LevelState.Paused;
            }
        }

        public void EndSimulation()
        {
            if (m_LevelState != LevelState.SimulationEnded)
            {
                m_PlayPauseToggleUI.interactable = false;
                m_PlayPauseToggleUI.isOn = false;

                if (OnSimulationEnd != null)
                    OnSimulationEnd();

                m_LevelState = LevelState.SimulationEnded;
            }
        }

        public void ToggleStepMode(bool enable)
        {
            m_StepMode = enable;
        }

        /// <summary>
        /// Should be called during initialization and StopSimulation()
        /// </summary>
        private void Init()
        {

            // TODO: initialize all objects in different way

            foreach (var cellObj in GetComponentsInChildren<CellObject>(true))
            {
                if (cellObj is PlayerBot)
                {
                    m_PlayerBot = cellObj as PlayerBot;
                    //Debug.Log("PlayerBot found");
                }

                cellObj.Initialize(this);
            }

            m_LevelState = LevelState.Ready;
            m_SimulationState = SimulationState.InitStep;
            m_CurStepTime = 0f;
            m_UpdateOnInitStep = false;

            if (m_StepIndicatorUI)
            {
                m_StepIndicatorUI.fillAmount = 0.0f;
            }
        }

        /// <summary>
        /// StepTick invokes OnStep() every frame when the state is Stepping and during end of BeginStep state
        /// </summary>
        private void StepTick()
        {
            m_CurStepTime += Time.deltaTime * stepSpeed;

            // step end condition
            if (m_CurStepTime >= 1f)
            {
                m_SimulationState = SimulationState.EndStep;
                return;
            }

            if (OnStepTick != null)
                OnStepTick(m_CurStepTime);
        }

        #endregion Simulation Functions

        private void SubscribeToLevelHandlerEvents()
        {
            LevelHandlerScene.OnLevelEnd += OnLevelEnd;
        }

        private void UnsubscribeFromLevelHandlerEvents()
        {
            LevelHandlerScene.OnLevelEnd -= OnLevelEnd;
        }

        private void OnLevelEnd()
        {
            StopSimulation();
        }
    }
}