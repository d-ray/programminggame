﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandCompareZero : BotCommand
    {
        public override string Name
        {
            get
            {
                return "COMP\nZER";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 1.0f); }
        }

        public override void OnExecuteEnd()
        {
            var val = m_CurrentBot.PopStack();
            if (val == 0)
                m_CurrentBot.PushStack(1);
            else
                m_CurrentBot.PushStack(0);

            base.OnExecuteEnd();
        }
    }
}