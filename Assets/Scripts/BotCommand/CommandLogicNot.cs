﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandLogicNot : BotCommand
    {
        public override string Name
        {
            get
            {
                return "NOT";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 0.25f, 0.75f); }
        }

        public override void OnExecuteEnd()
        {
            m_CurrentBot.PushStack(~m_CurrentBot.PopStack());

            base.OnExecuteEnd();
        }
    }
}