﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandWaitOne : BotCommand
    {
        public override string Name
        {
            get { return "WAIT\nONE"; }
        }

        public override Color Color
        {
            get { return new Color(0.0f, 1.0f, 0.25f); }
        }

        public override int StepCount
        {
            get { return 1; }
        }
    }
}