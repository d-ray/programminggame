﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandWait : BotCommand
    {
        private int m_StepCount;

        public override string Name
        {
            get { return "WAIT"; }
        }

        public override Color Color
        {
            get { return new Color(0.0f, 1.0f, 0.25f); }
        }

        public override int StepCount
        {
            get { return m_StepCount; }
        }

        public override void OnExecutionInit()
        {
            m_StepCount = m_CurrentBot.PopStack();
            if (m_StepCount == int.MinValue)
                m_StepCount = 0;

            base.OnExecutionInit();
        }
    }
}