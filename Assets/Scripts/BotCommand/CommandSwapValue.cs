﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandSwapValue : BotCommand
    {
        public override string Name
        {
            get
            {
                return "SWAP";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 0.25f); }
        }

        public override void OnExecuteEnd()
        {
            var a = m_CurrentBot.PopStack();
            var b = m_CurrentBot.PopStack();
            m_CurrentBot.PushStack(a);
            m_CurrentBot.PushStack(b);

            base.OnExecuteEnd();
        }
    }
}