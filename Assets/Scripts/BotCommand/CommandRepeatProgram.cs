﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandRepeatProgram : BotCommand
    {
        public override string Name
        {
            get { return "REPE\nPRG"; }
        }

        public override Color Color
        {
            get { return new Color(0.0f, 1.0f, 0.25f); }
        }

        public override void OnExecuteEnd()
        {
            m_CurrentBot.ProgramCounter = -1;

            base.OnExecuteEnd();
        }
    }
}