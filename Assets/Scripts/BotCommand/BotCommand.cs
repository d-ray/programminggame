﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{


    public abstract class BotCommand : MonoBehaviour
    {
        protected PlayerBot m_CurrentBot;
        protected CommandUI m_CommandUI;
        protected int? m_InternalValue;

        public virtual string Name
        {
            get { return "XXXX\nXXX"; }
        }

        public virtual Color Color
        {
            get { return Color.white; }
        }

        public virtual int StepCount
        {
            get { return 0; }
        }

        public PlayerBot CurrentBot
        {
            get { return m_CurrentBot; }
            set { m_CurrentBot = value; }
        }

        public CommandUI CommandUI
        {
            get { return m_CommandUI; }
        }

        public int? InternalValue
        {
            get { return m_InternalValue; }
            set { m_InternalValue = value; }
        }

        protected virtual void Start()
        {
            m_CommandUI = GetComponent<CommandUI>();
            Debug.Assert(m_CommandUI, "Command UI not found!", this);
            m_CommandUI.CommandText.text = Name;
            m_CommandUI.Backround.color = Color;
            //m_CommandUI.CursorMark.enabled = false;
            m_CommandUI.CursorMark.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            m_CommandUI.BreakpointMark.enabled = false;
        }

        private void Update()
        {
            m_CommandUI.CursorMark.color = new Color(1.0f, 1.0f, 1.0f, m_CommandUI.CursorMark.color.a - (6.0f * Time.deltaTime));
        }

        public virtual void OnDropInProgram() { }

        public virtual void OnExecutionInit() { }

        public virtual void OnExecuteBegin() { }

        // skipped when StepCount <= 0
        public virtual void OnExecuting(float progress)
        {
            m_CommandUI.CursorMark.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

        public virtual void OnExecuteEnd()
        {
            m_CommandUI.CursorMark.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }
}