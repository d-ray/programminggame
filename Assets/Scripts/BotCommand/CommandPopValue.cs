﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandPopValue : BotCommand
    {
        public override string Name
        {
            get
            {
                return "POP";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 0.25f); }
        }

        public override void OnExecuteEnd()
        {
            m_CurrentBot.PopStack();

            base.OnExecuteEnd();
        }
    }
}