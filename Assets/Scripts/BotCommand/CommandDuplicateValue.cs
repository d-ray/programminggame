﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandDuplicateValue : BotCommand
    {
        public override string Name
        {
            get
            {
                return "DUPE";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 0.25f); }
        }

        public override void OnExecuteEnd()
        {
            var val = m_CurrentBot.PopStack();
            m_CurrentBot.PushStack(val);
            m_CurrentBot.PushStack(val);

            base.OnExecuteEnd();
        }
    }
}