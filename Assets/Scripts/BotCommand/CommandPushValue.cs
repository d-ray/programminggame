﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProgrammingGame.UI;

namespace ProgrammingGame
{
    public class CommandPushValue : BotCommand
    {
        protected ModalWindowManager m_ModalManager;

        public override string Name
        {
            get
            {
                if (m_InternalValue == null)
                {
                    return "PUSH\nN";
                }

                return "PUSH\n" + m_InternalValue;
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 0.25f); }
        }

        protected override void Start()
        {
            base.Start();

            m_ModalManager = GetComponentInParent<LevelHandlerScene>().ModalWindowManager;
        }

        public override void OnDropInProgram()
        {
            m_ModalManager.ShowWindow<BotCommand>(0, this);
        }

        public override void OnExecuteEnd()
        {
            m_CurrentBot.PushStack((int)m_InternalValue);

            base.OnExecuteEnd();
        }
    }
}