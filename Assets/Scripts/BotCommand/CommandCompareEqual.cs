﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandCompareEqual : BotCommand
    {
        public override string Name
        {
            get
            {
                return "COMP\nEQU";
            }
        }

        public override Color Color
        {
            get { return new Color(1.0f, 1.0f, 1.0f); }
        }

        public override void OnExecuteEnd()
        {
            var a = m_CurrentBot.PopStack();
            var b = m_CurrentBot.PopStack();
            if (a == b)
                m_CurrentBot.PushStack(1);
            else
                m_CurrentBot.PushStack(0);

            base.OnExecuteEnd();
        }
    }
}