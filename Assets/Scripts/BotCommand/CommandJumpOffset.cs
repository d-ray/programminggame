﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandJumpOffset : BotCommand
    {
        public override string Name
        {
            get { return "JMP"; }
        }

        public override Color Color
        {
            get { return new Color(0.0f, 1.0f, 0.25f); }
        }

        public override void OnExecuteEnd()
        {
            var offset = m_CurrentBot.PopStack();
            if (offset != int.MinValue)
            {
                var absolute = m_CurrentBot.ProgramCounter + offset;
                if (absolute >= 0 && absolute < m_CurrentBot.CommandCount)
                {
                    m_CurrentBot.ProgramCounter = absolute - 1;
                }
                else
                {
                    m_CurrentBot.RaiseError();
                }
            }

            base.OnExecuteEnd();
        }
    }
}