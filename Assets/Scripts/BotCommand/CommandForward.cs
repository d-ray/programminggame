﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class CommandForward : BotCommand
    {
        public override string Name
        {
            get { return "MOVE\nFW"; }
        }

        public override Color Color
        {
            get { return new Color(0.0f, 0.75f, 1.0f); }
        }

        public override int StepCount
        {
            get { return 1; }
        }

        public override void OnExecuteBegin()
        {
            m_CurrentBot.MoveToDirection(MoveDirection.Forward);

            base.OnExecuteBegin();
        }
    }
}