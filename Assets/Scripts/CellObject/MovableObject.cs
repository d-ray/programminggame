﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using UnityEngine;
using ProgrammingGame;

namespace ProgrammingGame
{
    public enum RotateDirection
    {
        None,
        Left,
        Right,
    }

    public enum Direction
    {
        Invalid,
        Up,
        Left,
        Down,
        Right
    }

    public enum MoveDirection
    {
        None,
        Forward,
        Backward,
        Left,
        Right
    }

    public enum MoveType
    {
        Normal,
        BeingPushed
    }

    public class MovableObject : CellObject
    {
        [SerializeField]
        private int m_Weight = int.MaxValue;

        private Animation m_SpriteAnimation;

        private Direction m_Direction;

        private MoveDirection m_CurrentMoveDirection;
        private RotateDirection m_CurrentRotateDirection;

        private Vector3 m_DebugRayOrigin, m_DebugRayDirection;

        public int Weight
        {
            get { return m_Weight; }
        }

        public Direction Direction
        {
            get { return m_Direction; }
            set
            {
                m_Direction = value;
                transform.rotation.eulerAngles.Set(0.0f, 0.0f, GetAngleFromDirection(value));
            }
        }

        public override void Initialize(Level level)
        {
            m_SpriteAnimation = GetComponentInChildren<Animation>();

            if (m_SpriteAnimation == null)
            {
                Debug.Log("Animation component not found on object child.", this);
            }

            m_SpriteAnimation["obj_rot_idle"].layer = 1;
            m_SpriteAnimation["obj_left90"].layer = 1;
            m_SpriteAnimation["obj_right90"].layer = 1;

            m_Direction = GetDirectionFromAngle(transform.rotation.eulerAngles.z);
            //Debug.Log(m_Direction);

            m_CurrentMoveDirection = MoveDirection.None;
            m_CurrentRotateDirection = RotateDirection.None;

            base.Initialize(level);

            PlayAnimation("obj_idle");
            PlayAnimation("obj_rot_idle");
        }

        protected override void SubscribeToLevelEvents()
        {
            level.OnStepStart += OnStepStart;
            level.OnStepTick += OnStep;
            level.OnStepEnd += OnStepEnd;

            base.SubscribeToLevelEvents();
        }

        protected override void UnsubscribeFromLevelEvents()
        {
            level.OnStepStart -= OnStepStart;
            level.OnStepTick -= OnStep;
            level.OnStepEnd -= OnStepEnd;

            base.UnsubscribeFromLevelEvents();
        }

        // ROTATION STUFF

        public bool RotateToDirection(RotateDirection direction)
        {
            if (direction == RotateDirection.None)
            {
                return false;
            }

            m_CurrentRotateDirection = direction;

            switch (direction)
            {
                case RotateDirection.Left:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            m_Direction = Direction.Left;
                            break;
                        case Direction.Left:
                            m_Direction = Direction.Down;
                            break;
                        case Direction.Down:
                            m_Direction = Direction.Right;
                            break;
                        case Direction.Right:
                            m_Direction = Direction.Up;
                            break;
                        default:
                            break;
                    }
                    break;
                case RotateDirection.Right:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            m_Direction = Direction.Right;
                            break;
                        case Direction.Left:
                            m_Direction = Direction.Down;
                            break;
                        case Direction.Down:
                            m_Direction = Direction.Left;
                            break;
                        case Direction.Right:
                            m_Direction = Direction.Up;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            Debug.Log(m_Direction);

            return true;
        }

        // MOVEMENT HELPER FUNCTIONS

        private Vector3Int DirectionToVector3Int(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Forward:
                    return Vector3Int.RoundToInt(transform.up);
                case MoveDirection.Backward:
                    return Vector3Int.RoundToInt(-transform.up);
                case MoveDirection.Left:
                    return Vector3Int.RoundToInt(-transform.right);
                case MoveDirection.Right:
                    return Vector3Int.RoundToInt(transform.right);
                default:
                    break;
            }

            return Vector3Int.zero;
        }

        private Direction GetDirectionToObject(CellObject targetObject)
        {
            Vector2 diffPos = transform.position - targetObject.transform.position;
            var max = 0.0f;
            var dir = Direction.Invalid;

            if (diffPos == Vector2.zero)
            {
                return dir;
            }

            dir = diffPos.x > 0.0f ? Direction.Right : Direction.Left;

            max = Mathf.Abs(diffPos.x);

            if (Mathf.Abs(diffPos.y) > max)
            {
                dir = diffPos.y > 0.0f ? Direction.Up : Direction.Down;
            }

            return dir;
        }

        public static Direction GetDirectionFromAngle(float angle)
        {
            while (angle < 0.0f)
            {
                angle += 360.0f;
            }

            while (angle >= 360.0f)
            {
                angle -= 360.0f;
            }

            var result = Direction.Up;

            if (angle > 45.0f)
            {
                result = Direction.Left;
            }
            if (angle > 135.0)
            {
                result = Direction.Down;
            }
            if (angle > 225.0f)
            {
                result = Direction.Right;
            }
            if (angle > 315.0f)
            {
                result = Direction.Up;
            }

            return result;
        }

        public static float GetAngleFromDirection(Direction dir)
        {
            switch (dir)
            {
                case Direction.Left:
                    return 90.0f;
                case Direction.Down:
                    return 180.0f;
                case Direction.Right:
                    return 270.0f;
                default:
                    break;
            }

            return 0.0f;
        }

        private MoveDirection GetPushMoveDirection(Direction pushFrom)
        {
            var result = MoveDirection.None;

            switch (pushFrom)
            {
                case Direction.Up:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            result = MoveDirection.Backward;
                            break;
                        case Direction.Left:
                            result = MoveDirection.Left;
                            break;
                        case Direction.Down:
                            result = MoveDirection.Forward;
                            break;
                        case Direction.Right:
                            result = MoveDirection.Right;
                            break;
                        default:
                            break;
                    }
                    break;
                case Direction.Left:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            result = MoveDirection.Right;
                            break;
                        case Direction.Left:
                            result = MoveDirection.Backward;
                            break;
                        case Direction.Down:
                            result = MoveDirection.Left;
                            break;
                        case Direction.Right:
                            result = MoveDirection.Forward;
                            break;
                        default:
                            break;
                    }
                    break;
                case Direction.Down:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            result = MoveDirection.Forward;
                            break;
                        case Direction.Left:
                            result = MoveDirection.Right;
                            break;
                        case Direction.Down:
                            result = MoveDirection.Backward;
                            break;
                        case Direction.Right:
                            result = MoveDirection.Left;
                            break;
                        default:
                            break;
                    }
                    break;
                case Direction.Right:
                    switch (m_Direction)
                    {
                        case Direction.Up:
                            result = MoveDirection.Left;
                            break;
                        case Direction.Left:
                            result = MoveDirection.Forward;
                            break;
                        case Direction.Down:
                            result = MoveDirection.Right;
                            break;
                        case Direction.Right:
                            result = MoveDirection.Backward;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            return result;
        }

        // MOVEMENT STUFF

        public bool MoveToDirection(MoveDirection direction)
        {
            return MoveToDirection(direction, MoveType.Normal);
        }

        public bool MoveToDirection(MoveDirection direction, MoveType moveType)
        {
            if (direction == MoveDirection.None)
            {
                return false;
            }

            var fdir = DirectionToVector3Int(direction);
            var origin = Vector3Int.FloorToInt(transform.position);

            m_DebugRayOrigin = origin;
            m_DebugRayDirection = fdir;

            // if collide with wall
            if (!WallsCollisionCheck(origin, direction, moveType))
            {
                return false;
            }

            // if collide with movable
            if (!MovablesCollisionCheck(origin, direction, moveType))
            {
                return false;
            }

            m_CurrentMoveDirection = direction;

            transform.position += (Vector3)fdir;

            level.SyncCellObjectPosition(this);

            return true;
        }

        private void FloorsCollisionCheck(Vector3Int pos)
        {
            if (!level.FloorMap.HasTile(pos))
            {
                // TODO: The object will fall if no floor (destroy)
                SetDead();
                Debug.Log("Object " + name + " not on floor!", this);
            }
        }

        private bool WallsCollisionCheck(Vector3Int origin, MoveDirection direction, MoveType moveType)
        {
            return !level.WallMap.HasTile(origin + DirectionToVector3Int(direction)) ? true : ObjectCrushingCheck(moveType);
        }

        private bool MovablesCollisionCheck(Vector3Int origin, MoveDirection direction, MoveType moveType)
        {
            var other = level.GetCellObjectAt(origin + DirectionToVector3Int(direction));

            if (other == null)
            {
                return true;
            }

            if (other.Passable)
            {
                return true;
            }

            var otherMovable = other as MovableObject;

            if (otherMovable == null)
            {
                // unmovable object
                return false;
            }

            if (otherMovable.Weight <= Weight)
            {
                var dir = GetDirectionToObject(other);
                if (otherMovable.MoveToDirection(otherMovable.GetPushMoveDirection(dir), MoveType.BeingPushed))
                {
                    Debug.Log("Object " + name + " moving other movable.", this);
                    return true;
                }
                else
                {
                    return ObjectCrushingCheck(moveType);
                }
            }
            else
            {
                return ObjectCrushingCheck(moveType);
            }
        }

        private bool ObjectCrushingCheck(MoveType moveType)
        {
            if (moveType == MoveType.BeingPushed)
            {
                // TODO: Destroy this object when being crushed
                SetDead();

                Debug.Log("Object " + name + " being crushed!", this);

                return true;
            }
            else
            {
                Debug.Log("Object " + name + " movement blocked!", this);

                return false;
            }
        }

        // ANIMATION STUFF

        private void PlayAnimation(string name)
        {
            m_SpriteAnimation[name].speed = level.stepSpeed;
            m_SpriteAnimation.Play(name, PlayMode.StopSameLayer);
        }

        private void OnStepStart()
        {
            if (m_SpriteAnimation == null)
            {
                return;
            }

            var animName = string.Empty;

            switch (m_CurrentMoveDirection)
            {
                case MoveDirection.None:
                    animName = "obj_idle";
                    break;
                case MoveDirection.Forward:
                    animName = "obj_fwd";
                    break;
                case MoveDirection.Backward:
                    animName = "obj_back";
                    break;
                case MoveDirection.Left:
                    animName = "obj_left";
                    break;
                case MoveDirection.Right:
                    animName = "obj_right";
                    break;
                default:
                    break;
            }

            PlayAnimation(animName);

            switch (m_CurrentRotateDirection)
            {
                case RotateDirection.Left:
                    animName = "obj_left90";
                    break;
                case RotateDirection.Right:
                    animName = "obj_right90";
                    break;
                default:
                    break;
            }

            PlayAnimation(animName);
        }

        protected virtual void OnStep(float progress)
        {

        }

        private void OnStepEnd()
        {
            m_SpriteAnimation.Stop();

            float angle = 0;

            switch (m_CurrentRotateDirection)
            {
                case RotateDirection.Left:
                    angle = 90f;
                    break;
                case RotateDirection.Right:
                    angle = -90f;
                    break;
                default:
                    break;
            }

            while (angle < 0.0f)
            {
                angle += 360.0f;
            }

            while (angle > 360.0f)
            {
                angle -= 360.0f;
            }

            transform.Rotate(Vector3.forward, angle);

            PlayAnimation("obj_rot_idle");

            // check floor
            FloorsCollisionCheck(Vector3Int.FloorToInt(transform.position));

            m_CurrentMoveDirection = MoveDirection.None;
            m_CurrentRotateDirection = RotateDirection.None;
        }

        // DEBUG

        /*
        private void OnDrawGizmos()
        {
            var col = Gizmos.color;

            Gizmos.color = Color.yellow;

            Gizmos.DrawRay(m_DebugRayOrigin, m_DebugRayDirection);

            Gizmos.color = col;
        }
        */
    }
}
