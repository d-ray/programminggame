﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class PlayerBot : MovableObject
    {
        private RectTransform m_ProgramUI;
        private RectTransform m_BotStackUI;
        private GameObject m_StackValuePrefab;
        private List<BotCommand> m_BotCommandList;
        private Stack<StackValue> m_ValueStack;
        private int m_ProgramCounter;
        private int m_RemainingStep;

        public int ProgramCounter
        {
            get { return m_ProgramCounter; }
            set { m_ProgramCounter = value; }
        }

        public int CommandCount
        {
            get { return m_BotCommandList.Count; }
        }

        public override void Initialize(Level level)
        {
            m_ProgramUI = level.ProgramContentUI;
            m_BotStackUI = level.BotStackUI;
            m_StackValuePrefab = m_BotStackUI.GetComponent<BotStack>().StackValuePrefab;

            if (m_BotCommandList == null)
                m_BotCommandList = new List<BotCommand>();
            else
                m_BotCommandList.Clear();

            ClearStack();

            m_ProgramCounter = 0;
            m_RemainingStep = -1;

            base.Initialize(level);
        }

        public override void SetDead()
        {
            base.SetDead();

            level.EndSimulation();
        }

        protected override void SubscribeToLevelEvents()
        {
            level.OnSimulationPlay += OnSimulationPlay;
            level.OnPreStep += OnPreStep;
            level.OnPostStep += OnPostStep;

            base.SubscribeToLevelEvents();
        }

        protected override void UnsubscribeFromLevelEvents()
        {
            level.OnSimulationPlay -= OnSimulationPlay;
            level.OnPreStep -= OnPreStep;
            level.OnPostStep -= OnPostStep;

            base.UnsubscribeFromLevelEvents();
        }

        private void OnSimulationPlay()
        {
            for (int i = 0; i < m_ProgramUI.childCount; i++)
            {
                var cmd = m_ProgramUI.GetChild(i).GetComponent<BotCommand>();
                if (cmd)
                {
                    cmd.CurrentBot = this;
                    m_BotCommandList.Add(cmd);
                }
            }
        }

        private bool NextCommand()
        {
            m_ProgramCounter++;
            m_RemainingStep = -1;
            if (m_ProgramCounter >= m_BotCommandList.Count)
            {
                level.EndSimulation();
                return false;
            }

            return true;
        }

        public void PushStack(int val)
        {
            if (m_ValueStack.Count < 16)
            {
                var obj = Instantiate(m_StackValuePrefab, m_BotStackUI, false);
                obj.transform.SetAsLastSibling();

                var sv = obj.GetComponent<StackValue>();
                sv.Value = val;

                m_ValueStack.Push(sv);
            }
            else
            {
                level.EndSimulation();
            }
        }

        public int PopStack()
        {
            int val;
            if (m_ValueStack.Count > 0)
            {
                var sv = m_ValueStack.Pop();
                val = sv.Value;

                Destroy(sv.gameObject);
            }
            else
            {
                val = int.MinValue;
                level.EndSimulation();
            }

            return val;
        }

        public void RaiseError()
        {
            level.EndSimulation();
        }

        private void ClearStack()
        {
            if (m_ValueStack == null)
            {
                m_ValueStack = new Stack<StackValue>();
            }
            else
            {
                foreach (var stack in m_ValueStack)
                {
                    Destroy(stack.gameObject);
                }

                m_ValueStack.Clear();
            }
        }

        private void OnPreStep()
        {
            if (m_ProgramCounter < m_BotCommandList.Count)
            {
                if (m_RemainingStep == -1)
                {
                    m_BotCommandList[m_ProgramCounter].OnExecutionInit();
                    m_RemainingStep = m_BotCommandList[m_ProgramCounter].StepCount;
                }

                if (m_RemainingStep <= 0)
                {
                    m_BotCommandList[m_ProgramCounter].OnExecuteBegin();
                    m_BotCommandList[m_ProgramCounter].OnExecuteEnd();
                    if (NextCommand())
                        level.RepeatInitStep();
                }
                else
                {
                    m_BotCommandList[m_ProgramCounter].OnExecuteBegin();
                }

                return;
            }

            if (m_BotCommandList.Count == 0)
            {
                level.EndSimulation();
            }
        }

        protected override void OnStep(float progress)
        {
            if (m_ProgramCounter < m_BotCommandList.Count)
                m_BotCommandList[m_ProgramCounter].OnExecuting(progress);

            base.OnStep(progress);
        }

        private void OnPostStep()
        {
            if (m_ProgramCounter < m_BotCommandList.Count)
            {
                m_BotCommandList[m_ProgramCounter].OnExecuteEnd();
                m_RemainingStep--;
                if (m_RemainingStep <= 0)
                {
                    NextCommand();
                }
            }
        }
    }
}