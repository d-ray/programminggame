/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System;
using UnityEngine;

namespace ProgrammingGame
{
    /// <summary>
    /// The base class of all non-world level object
    /// </summary>
    public abstract class CellObject : MonoBehaviour
    {
        #region Variables

        /// <summary>
        /// Set to true if level events already subscribed by this object.
        /// </summary>
        private bool m_HasSubscribedToLevelEvents;

        /// <summary>
        /// Reference to the level object.
        /// </summary>
        protected Level level;

        protected Vector3? initialPosition;
        protected Quaternion? initialRotation;

        //protected bool isDead;

        public virtual bool Passable
        {
            get { return false; }
        }

        #endregion Variables

        #region Unity Methods

        /// <summary>
        /// Gets called when Level is initializing
        /// </summary>
        /// <param name="level">The current level</param>
        public virtual void Initialize(Level level)
        {
            // get the Level reference
            this.level = level;

            // subscribe to Level events
            if (!m_HasSubscribedToLevelEvents)
                SubscribeToLevelEvents();

            if (initialPosition == null)
                initialPosition = transform.position;
            else
                transform.position = (Vector3)initialPosition;

            if (initialRotation == null)
                initialRotation = transform.rotation;
            else
                transform.rotation = (Quaternion)initialRotation;

            // finally sync position with level
            level.SyncCellObjectPosition(this);

            gameObject.SetActive(true);

            //Debug.Log("CellObject initialization success", this);
        }

        public virtual void OnPass() { }

        public virtual void SetDead()
        {
            gameObject.SetActive(false);
            level.RemoveCellObject(this);
            UnsubscribeFromLevelEvents();
        }

        // Unsubscribe from level events
        protected virtual void OnDestroy()
        {
            if (m_HasSubscribedToLevelEvents)
                UnsubscribeFromLevelEvents();

            level.RemoveCellObject(this);
        }

        #endregion Unity Methods

        protected virtual void SubscribeToLevelEvents()
        {
            m_HasSubscribedToLevelEvents = true;
        }

        protected virtual void UnsubscribeFromLevelEvents()
        {
            m_HasSubscribedToLevelEvents = false;
        }
    }
}