﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using ProgrammingGame.UI;

namespace ProgrammingGame
{
    public class UICommandDropHandler : MonoBehaviour, IDropHandler
    {
        private RectTransform m_UIProgramViewTransform;
        private RectTransform m_UIProgramContentTransform;
        private GameObject m_UIProgramLockObject;
        private CommandUI m_DraggingUICommand;
        private LevelHandlerScene m_LevelHandler;

        public CommandUI DraggingUICommand
        {
            get { return m_DraggingUICommand; }
            set { m_DraggingUICommand = value; }
        }

        public void Start()
        {
            m_LevelHandler = GetComponentInParent<LevelHandlerScene>();
            m_UIProgramViewTransform = m_LevelHandler.UIProgramViewTransform;
            m_UIProgramContentTransform = m_LevelHandler.UIProgramContentTransform;
            m_UIProgramLockObject = m_LevelHandler.UIProgramLockObject;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (m_DraggingUICommand)
            {
                if (m_UIProgramViewTransform.gameObject.activeInHierarchy &&
                    !m_UIProgramLockObject.activeInHierarchy &&
                    RectTransformUtility.RectangleContainsScreenPoint(m_UIProgramViewTransform, eventData.position) &&
                    m_LevelHandler.CurrentCommandCount < m_LevelHandler.LevelMaxCommands)
                {
                    var index = m_UIProgramContentTransform.childCount;

                    for (int i = 0; i < m_UIProgramContentTransform.childCount; i++)
                    {
                        var content = m_UIProgramContentTransform.GetChild(i) as RectTransform;
                        if (RectTransformUtility.RectangleContainsScreenPoint(content, eventData.position))
                        {
                            index = i;
                            break;
                        }
                    }

                    m_DraggingUICommand.transform.SetParent(m_UIProgramContentTransform);
                    m_DraggingUICommand.transform.SetSiblingIndex(index);

                    if (!m_DraggingUICommand.HasBeenPlacedInProgram)
                    {
                        m_DraggingUICommand.BotCommand.OnDropInProgram();
                        m_DraggingUICommand.HasBeenPlacedInProgram = true;
                    }

                    m_LevelHandler.UpdateCommandCountText();
                }
                else
                {
                    Destroy(m_DraggingUICommand.gameObject);
                }

                DraggingUICommand = null;
            }
        }
    }
}