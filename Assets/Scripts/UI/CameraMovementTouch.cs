﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProgrammingGame
{
    public class CameraMovementTouch : MonoBehaviour
    {
        [SerializeField]
        private float speed = 35.0f;

        private Camera cam;
        private LineRenderer line;
        private Vector2 startPos;
        private Vector2 direction;
        private bool isDragging;

        private void Start()
        {
            cam = GetComponent<Camera>();
            line = GetComponent<LineRenderer>();
            isDragging = false;
        }

        private void Update()
        {
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                        {
                            startPos = touch.position;
                            isDragging = true;
                        }
                        break;

                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        if (isDragging)
                        {
                            direction = touch.position - startPos;
                            transform.Translate(cam.ScreenToViewportPoint(direction) * speed * Time.deltaTime, Space.World);

                            if (line)
                            {
                                line.widthMultiplier = 0.3f;
                                var wPosA = cam.ScreenToWorldPoint(startPos);
                                var wPosB = cam.ScreenToWorldPoint(touch.position);
                                wPosA.z = -9.0f;
                                wPosB.z = -9.0f;
                                line.SetPosition(0, wPosA);
                                line.SetPosition(1, wPosB);
                                var sin01 = (Mathf.Sin(Time.time * 6.0f) * 0.5f) + 0.5f;
                                line.startColor = Color.Lerp(new Color(0.0f, 0.5f, 1.0f), new Color(0.0f, 0.0f, 0.25f), sin01);
                                line.endColor = Color.Lerp(new Color(0.0f, 0.0f, 0.25f), new Color(0.0f, 0.5f, 1.0f), sin01);
                            }
                        }
                        break;

                    case TouchPhase.Ended:
                        if (line)
                        {
                            line.widthMultiplier = 0.0f;
                        }

                        isDragging = false;
                        break;

                    case TouchPhase.Canceled:
                        break;

                    default:
                        break;
                }
            }
        }
    }
}