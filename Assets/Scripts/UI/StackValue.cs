﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProgrammingGame
{
    public class StackValue : MonoBehaviour
    {
        [SerializeField]
        private Text m_ValueText;

        private int m_Value;

        public int Value
        {
            get { return m_Value; }
            set
            {
                m_Value = ByteIntUtils.CalculateOverflow(value);
                m_ValueText.text = m_Value.ToString();
            }
        }

        public void Start()
        {
            m_ValueText = GetComponentInChildren<Text>();
        }
    }
}