﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProgrammingGame.UI
{
    public class TabContentLinker : MonoBehaviour
    {
        [Serializable]
        private class TabContentLink
        {
            public Toggle tabToggle;
            public GameObject tabContent;
        }

        [SerializeField]
        private TabContentLink[] m_Links;
        [SerializeField]
        private GameObject m_DefaultContent;

        private void Update()
        {
            if (m_Links.Length > 0)
            {
                var hasOn = false;
                foreach (var link in m_Links)
                {
                    if (link.tabContent == null)
                        continue;

                    if (link.tabToggle.isOn)
                    {
                        link.tabContent.SetActive(true);
                        hasOn = true;
                    }
                    else
                    {
                        link.tabContent.SetActive(false);
                    }
                }

                if (m_DefaultContent != null)
                {
                    if (!hasOn)
                    {
                        m_DefaultContent.SetActive(true);
                    }
                    else
                    {
                        m_DefaultContent.SetActive(false);
                    }
                }
            }
        }
    }
}