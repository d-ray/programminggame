﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame
{
    public class BotStack : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_StackPrefab;

        public GameObject StackValuePrefab
        {
            get { return m_StackPrefab; }
        }
    }
}