﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ProgrammingGame
{
    public class CommandUI : MonoBehaviour, IBeginDragHandler, IDragHandler
    {
        [SerializeField]
        private Image background;

        [SerializeField]
        private Text commandText;

        [SerializeField]
        private Image cursorMark;

        [SerializeField]
        private Image breakpointMark;

        private int m_SiblingIndex;
        private bool m_HasBeenPlacedInProgram;
        private Transform m_CanvasObjectTransform;
        private UICommandDropHandler m_DropHandler;
        private LevelHandlerScene m_LevelHandler;
        private BotCommand m_BotCommand;

        public Image Backround { get { return background; } }
        public Text CommandText { get { return commandText; } }
        public Image CursorMark { get { return cursorMark; } }
        public Image BreakpointMark { get { return breakpointMark; } }

        public BotCommand BotCommand { get { return m_BotCommand; } }

        public bool HasBeenPlacedInProgram
        {
            get { return m_HasBeenPlacedInProgram; }
            set { m_HasBeenPlacedInProgram = value; }
        }

        private void DragUpdate(Vector2 position)
        {
            transform.position = position;
        }

        private void Start()
        {
            m_HasBeenPlacedInProgram = false;
            m_CanvasObjectTransform = GetComponentInParent<Canvas>().transform;
            m_DropHandler = GetComponentInParent<UICommandDropHandler>();
            m_LevelHandler = GetComponentInParent<LevelHandlerScene>();
            m_BotCommand = GetComponent<BotCommand>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            m_SiblingIndex = transform.GetSiblingIndex();

            if (!m_HasBeenPlacedInProgram)
            {
                var obj = Instantiate(gameObject, transform.parent);
                obj.transform.SetSiblingIndex(m_SiblingIndex);
                obj.name = gameObject.name;
            }

            transform.SetParent(m_CanvasObjectTransform, true);
            m_DropHandler.DraggingUICommand = this;
            m_LevelHandler.UpdateCommandCountText();

            DragUpdate(eventData.position);
        }

        public void OnDrag(PointerEventData eventData)
        {
            DragUpdate(eventData.position);
        }
    }
}