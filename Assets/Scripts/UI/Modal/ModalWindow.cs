﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame.UI
{
    public abstract class ModalWindow : MonoBehaviour
    {
        private ModalWindowManager m_WindowManager;

        protected virtual void Start()
        {
            m_WindowManager = GetComponentInParent<ModalWindowManager>();
        }

        public virtual void OnShow<T>(T obj)
        {
            gameObject.SetActive(true);
        }

        protected virtual void OnClose()
        {
            m_WindowManager.CloseWindow();
        }

        public virtual void OnAccept()
        {
            OnClose();
        }

        public virtual void OnCancel()
        {
            OnClose();
        }
    }
}