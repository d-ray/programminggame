﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame.UI
{
    public class ConfirmExitWindow : ModalWindow
    {
        public override void OnAccept()
        {
            base.OnAccept();

            Application.Quit();
            Debug.Log("Quitting");
        }
    }
}