﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame.UI
{
    public class ConfirmMainMenuWindow : ModalWindow
    {
        private LevelHandlerScene m_LevelHandler;

        protected override void Start()
        {
            base.Start();

            m_LevelHandler = GetComponentInParent<LevelHandlerScene>();
        }

        public override void OnAccept()
        {
            base.OnAccept();

            m_LevelHandler.GoToScene(BaseScene.Scene.MainMenu);
        }
    }
}