﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingGame.UI
{
    public class ConfirmClearProgramWindow : ModalWindow
    {
        private RectTransform m_UIProgramContentTransform;

        protected override void Start()
        {
            base.Start();

            m_UIProgramContentTransform = GetComponentInParent<LevelHandlerScene>().UIProgramContentTransform;
        }

        public override void OnAccept()
        {
            for (int i = 0; i < m_UIProgramContentTransform.childCount; i++)
            {
                Destroy(m_UIProgramContentTransform.GetChild(i).gameObject);
            }

            base.OnAccept();
        }
    }
}