﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProgrammingGame.UI
{
    public class NumberInputWindow : ModalWindow
    {
        [SerializeField]
        private Text m_NumberPreviewText;

        private int m_Value;
        private BotCommand m_CurrentCommand;

        private void UpdateText()
        {
            m_NumberPreviewText.text = m_Value.ToString();
        }

        private void ClampValue()
        {
            if (m_Value > 127)
            {
                m_Value = 127;
            }

            if (m_Value < -128)
            {
                m_Value = -128;
            }
        }

        public override void OnShow<T>(T obj)
        {
            base.OnShow(obj);

            m_CurrentCommand = obj as BotCommand;
            m_Value = 0;
            UpdateText();
        }

        public override void OnAccept()
        {
            m_CurrentCommand.InternalValue = m_Value;
            m_CurrentCommand.CommandUI.CommandText.text = m_CurrentCommand.Name;

            base.OnAccept();
        }

        public override void OnCancel()
        {
            Destroy(m_CurrentCommand.gameObject);

            base.OnCancel();
        }

        public void NumberInput(int value)
        {
            m_Value *= 10;
            if (m_Value >= 0)
            {
                m_Value += value;
            }
            else
            {
                m_Value -= value;
            }

            ClampValue();
            UpdateText();
        }

        public void NumberNegate()
        {
            m_Value *= -1;

            ClampValue();
            UpdateText();
        }

        public void NumberDelete()
        {
            m_Value /= 10;

            UpdateText();
        }
    }
}