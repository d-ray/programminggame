﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProgrammingGame.UI
{
    public class ModalWindowManager : MonoBehaviour
    {
        [SerializeField]
        private List<ModalWindow> m_WindowList;

        private RawImage m_ScreenLockImage;
        private ModalWindow m_CurrentShownWindow;

        public void Start()
        {
            m_ScreenLockImage = GetComponent<RawImage>();

            // test
            //ShowWindow(0);
        }

        public bool IsShowing
        {
            get { return m_CurrentShownWindow != null; }
        }

        public void ShowWindow(int index)
        {
            ShowWindow<object>(index, null);
        }

        public void ShowWindow<T>(int index, T obj)
        {
            if (m_CurrentShownWindow)
                return;

            m_ScreenLockImage.enabled = true;
            m_CurrentShownWindow = m_WindowList[index];
            m_CurrentShownWindow.OnShow(obj);
        }

        public void CloseWindow()
        {
            m_CurrentShownWindow.gameObject.SetActive(false);
            m_CurrentShownWindow = null;
            m_ScreenLockImage.enabled = false;
        }
    }
}