/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace ProgrammingGame
{
    /// <summary>
    /// The base of all scene class
    /// </summary>
    public class BaseScene : MonoBehaviour
    {
        #region Constants

        /// <summary>
        /// Scene enum
        /// </summary>
        public enum Scene
        {
            MainMenu,
            LevelHandler
        }

        #endregion Constants

        #region Unity Methods

        protected virtual void Awake()
        {
            // instantiate singleton components here!
            Session.Instantiate();
        }

        /// <summary>
        /// Switches to another scene by enum
        /// </summary>
        /// <param name="scene"></param>
        public void GoToScene(Scene scene)
        {
            SceneManager.LoadScene(scene.ToString());
        }

        /// <summary>
        /// Switches to another scene by name
        /// </summary>
        /// <param name="sceneName"></param>
        public void GoToScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        #endregion Unity Methods
    }
}