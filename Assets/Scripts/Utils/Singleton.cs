/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using UnityEngine;

namespace ProgrammingGame
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T m_Instance;

        public static T Instance
        {
            get
            {
                return m_Instance ?? (m_Instance = SingletonManager.GOInstance.AddComponent(typeof(T)) as T);
            }
        }

        public static void Instantiate()
        {
            m_Instance = Instance;
        }

        public Singleton()
        {
            m_Instance = this as T;
        }
    }

    public static class SingletonManager
    {
        private static GameObject m_GameObject;

        public static GameObject GOInstance
        {
            get
            {
                if (m_GameObject == null)
                {
                    m_GameObject = new GameObject("Singleton");
                    Object.DontDestroyOnLoad(m_GameObject);
                }

                return m_GameObject;
            }
        }
    }
}