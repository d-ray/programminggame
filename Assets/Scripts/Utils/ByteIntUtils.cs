﻿/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ByteIntUtils
{
    public static int CalculateOverflow(int val)
    {
        while (val > 127)
        {
            val -= 256;
        }

        while (val < -128)
        {
            val += 256;
        }

        return val;
    }
}
