/*
* Copyright (c) Indra Yudaprawira
* indray@outlook.com
*/

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

namespace ProgrammingGame
{
    [System.Serializable]
    public class GameSessionData
    {
        public int currentLevel;

        public GameSessionData()
        {
            currentLevel = 0;
        }
    }

    /// <summary>
    /// Manages the game session
    /// </summary>
    public class Session : Singleton<Session>
    {
        #region Variables

        private int m_LevelCount;
        private GameSessionData m_SessionData;

        #endregion Variables

        public int LevelCount
        {
            get { return m_LevelCount; }
            set { m_LevelCount = value; }
        }

        public int CurrentLevel
        {
            get { return m_SessionData.currentLevel; }
            set { m_SessionData.currentLevel = value; }
        }

        public bool SessionLoaded
        {
            get { return m_SessionData != null; }
        }

        #region Unity Methods

        #endregion Unity Methods

        public void PlayNext()
        {
            m_SessionData.currentLevel++;
        }

        public bool HasNext()
        {
            return m_SessionData.currentLevel + 1 < m_LevelCount;
        }

        public void NewSession()
        {
            m_SessionData = new GameSessionData();

            SaveSession();
        }

        public void DeleteSession()
        {
            m_SessionData = null;

            var path = Path.Combine(Application.persistentDataPath, "current.ses");
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public void SaveSession()
        {
            var bf = new BinaryFormatter();
            var path = Path.Combine(Application.persistentDataPath, "current.ses");
            var fs = File.Create(path);
            bf.Serialize(fs, m_SessionData);
            fs.Close();
            Debug.Log("Wrote session data to " + path);
        }

        public void LoadSession()
        {
            var path = Path.Combine(Application.persistentDataPath, "current.ses");
            if (File.Exists(path))
            {
                var bf = new BinaryFormatter();
                var fs = File.Open(path, FileMode.Open);
                m_SessionData = (GameSessionData)bf.Deserialize(fs);
                fs.Close();
                Debug.Log("Read session data from " + path);
            }
        }
    }
}